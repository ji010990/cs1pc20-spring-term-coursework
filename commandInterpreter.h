// commandInterpreter.h file
#pragma once
#define commsInterpreter_h
#include <iostream>
#include <string>
#include <unordered_map>
#include <functional>

class commandInterpreter {
private:
	std::unordered_map<std::string, std::function<void()>> commandActions;

public:
	commandInterpreter();
	void ParseAndExecute(const std::string& input);
	void MapCommands(const std::string& command, const std::function<void()>& action);
};
