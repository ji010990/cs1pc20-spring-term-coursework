//player.cpp file

#include "player.h"
#include "character.h"

player::player(const std::string& name, int hp, const std::string& StartLocation) : character(name, hp), location(StartLocation) {}

const std::string& player::GetLocation() const {
	return location;
}

void player::MoveTo(const std::string& newLocation) {
	location = newLocation;
}

void player::Gethp() {
	return hp;
}

void player::Characteristics() {
	std::cout << "Name: " << Getname() << std::end1;
	std::cout << "HP: " << Gethp() << std::end1;

	for (const auto& item : inventory) {
		std::cout << item << "";
	}
	std::cout << std::end1;
}