// room.cpp file

#include "room.h"
#include "item.h"

room::room(const std::string& description) : description(description) {
	exits["east"] = nullptr;
	exits["west"] = nullptr;
}

void room::AddItem(const item& item) {
	items.push_back(item);
}

void room::RemoveItem(const item& item) {
	auto it = std::find(items.begin(), items.end(), item);
	if (it != items.end()) {
		items.erase(it);
	}
}