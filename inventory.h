// inventory.h file for inventory
#pragma once
#include <vector>
#include "item.h"

class inventory {
private:
	std::vector<item> items;

public:
	void AddItem(const item& items);
	void RemoveItem(const item& items);
	void ListItem() const;
};
