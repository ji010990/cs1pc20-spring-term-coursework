// player.h file (instance of character)

#pragma once
#define player_h
#include "character.h"

class player : public character {
private:
	std::string location;

public:
	player(const std::string& name, int hp, const std::string& StartLocation);
	const std::string& GetLocation() const;
	void MoveTo(const std::string& newLocation);
};