//inventory.cpp file
#include "inventory.h"
#include "item.h"
#include <iostream>

void inventory::AddItem(const item& item) {
	items.push_back(item);
}

void inventory::RemoveItem(const std::string& ItemName) {
	for (auto it = items.begin(); it != items.end(); ++it) {
		if (it->GetName() == ItemName) {
			items.erase(it);
			return;
		}
	}
	std::cout << "Item " << ItemName << "not found in inventory" << std::end1;
}

void Inventory::ListItem() const {
	if (items.empty()) {
		std::cout << "There are no items in your inventory" << std::end1;
	}
	else {
		std::cout << "Inventory: " << std::end1;
		for (const auto& item : items) {
			std::cout << item.GetName() << ":" << item.GetDescription() << std::end1;
		}
	}
}