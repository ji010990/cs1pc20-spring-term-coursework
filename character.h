// character.h file
// hp = health

#pragma once
#define character_h

#include "item.h"
#include <iostream>
#include <string>
#include <vector>

class character {
private:
	std::string name;
	std::vector<item> inventory;
	int hp;

public:
	character(const std::string& name, int hp);
	void TakeDamage(int damage);
};

class player : public character {
private:
	std::string location;

public:
	player(const std::string& name, int hp, const std::string StartLocation);
	const std::string& GetLocation() const;
	void MoveTo(const std::string& newLocation);
};