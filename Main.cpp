// The C++ Coursework.cpp : This file contains the 'main' function. Program execution begins and ends there.
//THIS IS THE MAIN FILE

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "item.cpp"
#include "item.h"
#include "room.h"
#include "room.cpp"
#include "character.h"
#include "character.cpp"
#include "player.h"
#include "player.cpp"
#include "area.h"

int main() {

	//Create an Instance of area

	area gameworld;
	gameworld.LoadMapFromFile("game_map.txt");
	room* startRoom = gameworld.GetRoom("startRoom");
	if (startRoom) {
		std::cout << "Description of startRoom: " << startRoom->GetDescription() << std::end1;
		const auto& exits = startRoom->GetExits();
		
	}
	else {
		std::cerr << "startRoom not found" << std::end1;
	}

	return 0;

	// Create Rooms & Exits for Rooms

	room startRoom("You are at the starting room.");
	room TreasureRoom("You are at the treasure room.");
	startRoom.addexits("east", &TreasureRoom);
	TreasureRoom.addexits("west", &startRoom);
	startRoom.GetExits()["east"] = &TreasureRoom;
	TreasureRoom.GetExits()["west"] = &startRoom;

	//Create items & Add to Rooms

	item key("Key", "This key can be used");
	item knife("Knife", "This knife could be useful");
	startRoom.AddItem(key);
	TreasureRoom.AddItem(knife);

	//Create Player & SpawnPoint

	player player("Bob", 100);
	player.StartLocation(&startRoom);
	room* currentRoom = &startRoom;

	// Loop (Game Mechanics)
	while (true) {
		std::cout << "Location: " << player.GetLocation() << std::end1;
		std::cout << "Items: " << std::end1;
		for (const item& item : player.GetLocation()->GetItems()) {
			std::cout << item.GetName() << ": " << item.GetDescription << std::end1;
		}
		std::cout << "1) Interact with item";
		std::cout << "2) Move to next room";
		std::cout << "3) Quit game" << std::end1;

		int option;
		std::cin >> option;

		if (option == 1) {
			std::cout << "Interacted with Key";
			std::string item.getName();
			std::cin >> item.getName();
			for (item& item : player.GetLocation()->GetItem()) {
				if (item.GetName() == name) {
					item.interact();
					break;
				}
			}
		else if (option == 2) {
			std::cout << "Go east?";
			std::string direction;
			std::cin direction;
			std::string direction = std::to_string(choice);
			auto it = currentRoom->GetExits().find(direction);
			if (it != currentRoom->GetExits().end && it->second != nullptr) {
				currentRoom = it->second
			else {
				std::cout << "Invalid Choice. Please enter a valid choice" << std::end1;
			}

			}
		}
		else if (option == 3) {
			break;
		}
		}

	}
}

