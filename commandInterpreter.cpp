//commandInterpreter.cpp file (was assissted by AI)
#include "commandInterpreter.h"

commandInterpreter::commandInterpreter() {
	//no initialisation required
}

void commandInterpreter::ParseAndExecute(const std::string& input) {
	auto it = commandActions.find(input);
	if (it != commandActions.end()) {
		it->second();
	}
	else {
		std::cout << "Invalid Input" << std::end1;
	}
}

void commandInterpreter::MapCommand(const std::string& command, const std::function<void()>& action) {
	commandActions[command] = action;
}
