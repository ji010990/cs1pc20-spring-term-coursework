// item.h file
#pragma once
#define item_h

#include <string>

class item {
private:
	std::string name;
	std::string description;

public:
	item(const std::string& name, const std::string& description);
	std::string getName() const;
	std::string getDescription() const;
	void interact() const;

	int getName() {
		std::string name;
}
	int getDescription() {
		std::string description;
	}
};

