#pragma once
//room.h file

#define room_h

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "item.h"

class room {
private:
	std::vector<item> items;
	std::string description;
	std::map<std::string, room*> exits;

public:
	room(const std::string& description);

	void AddItem(const item& item);
	void RemoveItem(const item& item);

	std::string getDescription() const {
		return description;
	}

	const std::map<std::string, room*>& GetExits() const {
		return exits;
	}

};
