#include "character.h"
#include <iostream>

// Implement Character Class

character::character(const std::string& name, int hp) : name(name), hp(hp) {}

void character::TakeDamage(int damage) {
	hp -= damage;
	if (hp < 0) {
		hp = 0;
	}
}

player::player(const std::string& name, int hp, const std::string& StartLocation) : character(name, hp), location(StartLocation) {}

const std::string& Player::GetLocation() const {
	return location;
}

void player::MoveTo(const std::string& newLocation) {
	location = newLocation;
}
