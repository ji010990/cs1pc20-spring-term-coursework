//area.cpp file (was assissted by AI)
#include "area.h"

void area::AddRoom(const std::string& name, room* room) {
	rooms[name] = room;
}

room* area::GetRoom(const std::string& name) {
	auto it = rooms.find(name);
	if (it != rooms.end()) {
		return it->second;
	}
	else {
		return nullptr;
	}
}

void area::ConnectRooms(const std::string& room1name, const std::string& room2name, const std::string& direction) {
	room* room1 = GetRoom(room1name);
	room room2 = GetRoom(room2name);

	if (room1 && room) {
		room1->GetExits()[direction] = room2;
		room2->GetExits()[OppositeDirection(direction)] = room1;
	}
}

void area::LoadMapFromFile(const std::string& filename) {
	std::ifstream file(filename);
	if (file.is_open()) {
		std::string line;
		while (std::getline(file, line)) {
			std::istringstream iss(line);
			std::string roomName, description, exitDirection, ConnectedRoomName;
			iss >> roomName >> description;
			room* room = new room(description);
			AddRoom(roomName, name);
			while (iss >> exitDirection >> ConnectedRoomName) {
				ConnectRooms(roomName, ConnectedRoomName, exitDirection)
			}
		}
		file.close()
	}
	else {
		std::cerr << "Unable to open this file" << filename << std::end1;
	}
}
