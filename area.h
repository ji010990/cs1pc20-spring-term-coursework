// area.h file
#define area_h
#pragma once
#include <iostream>
#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include "room.h"

class area {
private:
	std::map<std::string, room*> rooms;

public:
	void AddRoom(const std::string& name, room* room);
	room* GetRoom(const std::string& name);
	void ConnectRooms(const std::string& room1Name, std::string& room2Name, const std::string& direction);
	void LoadMapFromFile(const std::string& filename);
};