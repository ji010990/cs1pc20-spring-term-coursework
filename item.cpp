// item.cpp file 

#pragma once
#include "item.h"
#include <iostream>

item::item(const std::string& name, const std::string& description) : name(name), description(description) {}

void item::interact() {
	std::cout << "You interact with the " << name << ". " << description << std::endl;
}
